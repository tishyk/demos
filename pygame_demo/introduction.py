import pygame
# original article - https://pythonprogramming.net/pygame-python-3-part-1-intro/

pygame.init()

"""
define our game's display, which is the main "display" for our game. 
You may also see this referred to as a "surface," as this is basically our canvas that we will draw things to, 
and the function literally returns a pygame.Surface object. 
We are saying right now that we want the resolution of our game to be 800 px wide and 600 px tall. 
Take note that this is a tuple as a function argument. 
If you do not make this a tuple with parenthesis, then 600 and 800 will be treated as separate parameters and the function will blow up.
"""

gameDisplay = pygame.display.set_mode((800, 600))
pygame.display.set_caption('A bit pygame')

"""
this is a our game clock. We use this to track time within the game, and this is mostly used for FPS, or "frames per second." 
While somewhat trivial seeming, FPS is very important, and can be tweaked as we will see later. 
For the most part, the average human eye can see ~30 FPS. 
It's important to note, however, that this is only a very general statement, since every human eye is slightly different, 
and the human eye does not process things in "frames." The better way to put it is that after about 30 FPS, 
people generally cannot tell the difference."""
clock = pygame.time.Clock()

"""
first, we've got a crashed = False statement, which is just a variable that we set initially.
Then, we run our "game loop," which will run until we crash.
Currently, the only way we're saying crashed = True is if the user exits out of the window, however.

You'll notice here that we have a for loop within this while loop.
This is going to be present in most PyGame scripts, where events are constantly being logged. 
It is shown in the video, but not here, but you can still try it: Try adding "print event" above the if statement. 
You will see in your console everything you do within the PyGame window. Pretty neat!

After our if statement. you'll see that we run a pygame.display.update. 
It's important to note the difference between display.update and display.flip. Display.flip will update the entire surface. 
Basically the entire screen. Display.update can just update specific areas of the screen. 
That said, if you do not pass a parameter, then update will update the entire surface as well, 
basically making flip() pointless for our interests.
There might come times when you want to use flip for very specific tasks, however.
"""
crashed = False

while not crashed:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True

        print(event)

    pygame.display.update()
    clock.tick(60)

"""
Once we have broken our game loop, we want to run a pygame.quit(). This will end our pygame instance.
Then we can run a simple quit(), which will exit Python and the application.
"""
pygame.quit()
quit()
