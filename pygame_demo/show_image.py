import pygame

"""
In this PyGame tutorial, we cover how to display custom-created game images to the screen.
You can also draw objects to the screen using coordinates, which we will cover later.
Here, we've created some award-winning graphics that we definitely want in our game,
so we want to know how to get it to show up.
"""



pygame.init()


display_width = 800
display_height = 600

gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('A bit pygame')

black = (0,0,0)
white = (255,255,255)

clock = pygame.time.Clock()
crashed = False

# Here, we load the /src/png/car2.png image to our carImg variable.
carImg = pygame.image.load('./src/png/car2.png')

# You can scale the image with pygame.transform.scale
carImg = pygame.transform.scale(carImg, (320, 150))

# You can then get the bounding rectangle of picture with
rect = carImg.get_rect()
# and move the picture with
rect = rect.move((10, 20))


"""
 Define our car function, which really just places the car to the display. 
 "Blit" basically just draws the image to the screen, but we still have yet to fully show it to the display. 
 In graphics, generally, there is a lot done in the background, and only 
 when every update is done is when the screen is visually updated.
"""

def car(x,y):
    gameDisplay.blit(carImg, (x ,y)) # or gameDisplay.blit(carImg, rect)


"""Here, we've defined the starting points for our car. 
See how we used out previously-defined variables? 
We're already seeing how this could be useful, especially if we EVER want to change our resolution. 
A lot of people start out writing programs, thinking that some of the variables will just never be changed."""

x =  (display_width * 0.05)
y = (display_height * 0.15)

while not crashed:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True
    """
    Here, we've filled our display with a color, white. 
    What this does is cover everything in white. Paint the game white, so-to-speak. 
    This will cover over any existing stuff. After that, we run our car function to draw the car to the screen. 
    Always keep in mind the order here. 
    If you drew the car first, then called the gameDisplay.fill function, you would have just covered your car and it would not show up.
    """
    gameDisplay.fill(white)
    car(x,y)


    pygame.display.update()
    clock.tick(60)

pygame.quit()
quit()